# A set of Jupyter notebooks as part of the M2R course hydrodynamic lmit of interacting particle system

**Authors:** Xiaolin Zeng

## Installation

### Using Conda

#### Install Anaconda

Follow instructions for installing [Anaconda](https://www.anaconda.com/distribution/#download-section) for python 3.

#### Install dependencies

From project root directory, run:

```bash
conda env create -f environment.yml
```

> **Note:** the `Solving environment` step may be long, be patient...

## Usage

### Activate environment

Before first execution, run:

```bash
conda activate hydro22
```

### Run within Jupyter notebooks

From project root directory, run:

```bash
jupyter-notebook
```

### Launch python scripts from command line

From project root directory, run:

```bash
python srw.py
```

## Test with Binder

[![Binder](https://static.mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fzeng%2Fhydro22/master)
